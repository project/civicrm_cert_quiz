<?php
/**
 * @file
 * civicrm_cert_quiz.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function civicrm_cert_quiz_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_quiz_cert|certification_rule|certification_rule|form';
  $field_group->group_name = 'group_quiz_cert';
  $field_group->entity_type = 'certification_rule';
  $field_group->bundle = 'certification_rule';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Advanced Quiz Integration',
    'weight' => '3',
    'children' => array(
      0 => 'field_cert_quiz_end',
      1 => 'field_cert_quiz_start',
      2 => 'field_cert_quiz_ref',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-quiz-cert field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_quiz_cert|certification_rule|certification_rule|form'] = $field_group;

  return $export;
}
