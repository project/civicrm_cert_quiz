<?php
/**
 * @file
 * civicrm_cert_quiz.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function civicrm_cert_quiz_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'certification_rule-certification_rule-field_cert_quiz_end'
  $field_instances['certification_rule-certification_rule-field_cert_quiz_end'] = array(
    'bundle' => 'certification_rule',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_integer',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'certification_rule',
    'field_name' => 'field_cert_quiz_end',
    'label' => 'Certification Quiz End Hours',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 37,
    ),
  );

  // Exported field_instance: 'certification_rule-certification_rule-field_cert_quiz_ref'
  $field_instances['certification_rule-certification_rule-field_cert_quiz_ref'] = array(
    'bundle' => 'certification_rule',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'certification_rule',
    'field_name' => 'field_cert_quiz_ref',
    'label' => 'Certification Quiz',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 35,
    ),
  );

  // Exported field_instance: 'certification_rule-certification_rule-field_cert_quiz_start'
  $field_instances['certification_rule-certification_rule-field_cert_quiz_start'] = array(
    'bundle' => 'certification_rule',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'number',
        'settings' => array(
          'decimal_separator' => '.',
          'prefix_suffix' => TRUE,
          'scale' => 0,
          'thousand_separator' => ' ',
        ),
        'type' => 'number_integer',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'certification_rule',
    'field_name' => 'field_cert_quiz_start',
    'label' => 'Certification Quiz Start Hours ',
    'required' => 0,
    'settings' => array(
      'max' => '',
      'min' => '',
      'prefix' => '',
      'suffix' => '',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'number',
      'settings' => array(),
      'type' => 'number',
      'weight' => 36,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Certification Quiz');
  t('Certification Quiz End Hours');
  t('Certification Quiz Start Hours ');

  return $field_instances;
}
