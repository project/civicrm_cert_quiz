<?php
/**
 * @file
 * civicrm_cert_quiz.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function civicrm_cert_quiz_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
}
